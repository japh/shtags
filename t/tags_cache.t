#!/usr/bin/env perl

# Unit tests against the 'perl' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/lib";     # for Shtags::Test
use lib "$RealBin/..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => "read tags from existing 'tags' file",
                 directory => {
                              'file1.pl' => q{package Foo;},
                              tags       => qq{tag1\tfile1\t/^tag1/},
                              },
                 expect    => {
                              tags  => qq{
                                         !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                         !_TAG_FILE_SORTED\t2\tcase insensitive order
                                         Foo\tfile1.pl\t/^package\\s\\+Foo;/
                                         },
                              },
                 );

$tester->run_test(
                 test_name => "delete tags file if there are no tags",
                 directory => {
                              tags => q{# just some junk},
                              },
                 expect    => {
                              },
                 );

$tester->run_test(
                 test_name => "discard old tags when parsing a missing file",
                 cli_args  => [ 'dir1/file1' ],
                 directory => {
                              dir1 => {
                                      file2 => q{tag2},
                                      },
                              tags => qq{
                                        !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                        !_TAG_FILE_SORTED\t2\tcase insensitive order
                                        tag1\tdir1/file1\t/^tag1/
                                        tag2\tdir1/file2\t/^tag2/
                                        },
                              },
                 expect    => {
                              dir1 => {
                                      file2 => {},
                                      },
                              tags => qq{
                                      !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                      !_TAG_FILE_SORTED\t2\tcase insensitive order
                                      tag2\tdir1/file2\t/^tag2/
                                      },
                              },
                 );

exit;
