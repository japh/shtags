package Shtags::Tester;

# test driver for unit testing 'shtags'
#
# this module runs a set of standardised tests against an instance of a
# Shtags::Mock object
#
#   Differences between Shtags and Shtags::Mock:
#
#       no file system interaction
#
#           all files and directories must be provided as data structure by
#           the test script.
#
#           a 'tags' file is neither read or written from the file system
#

use strict;
use warnings;

use Test::More;
use Test::Differences;

use Shtags::Mock;
use Shtags::Test::FileSystem;

sub new
    {
    my $class = shift;
    my $self  = bless {}, $class;
    return $self;
    }

sub DESTROY
    {
    done_testing();
    }

# data-centric test, common to all language parsers etc.
#
# run_test()
#
# Parameters:
#
#   {test_name}     Required name of the test.
#                   Each call to run_test() should test 1 aspect of the system
#                   and should have a meaningful name.
#
#   {cli_args}      A list of CLI arguments (directory and file names to be indexed)
#                   Set this in order to test behaviour of only re-indexing
#                   part of a directory tree.
#
#   {directory}     An in-memory, hash-representation of a file system
#                   embedded hashes represent directories
#                   scalar values (multi-line strings) are files.
#
#                   The 'current working directory' of each test is implicitly
#                   the top of the {directory} tree.
#
#                   Indenting:
#                       - all indenting, up-to-and-including an optional '|' marker,
#                         is removed automatically
#                       - leading and trailing empty lines are also removed
#
#   {expect}        Also a tree of file specifications, but traversal of the
#                   expect tree is bound to the tree from {directory} in order
#                   to determine the type of each item.
#
#                   Each expectation represents a test which can be performed
#                   against the 'file':
#
#                       language    The name of the parser which was selected for the file.
#                                   undef indicates that the file was ignored by shtags
#
#                       tag_names   An array of tags which should have been discovered.
#                                   Order is not important, but case is!
#
#                   All paths in the {expect} tree MUST also exist in the
#                   {directory} tree.
#
#                   The only exception (special case) to this is a 'tags' file
#                   in the top-level hash.
#
#                   If a 'tags' file exists in either {directory} or {expect}
#                   then the resulting tags file will be checked.
#
#                   If a 'tags' file is found in {directory}, it will be read
#                   before parsing any other files.
#                   If it should have been created or modified, it must be in {expect}
#                   If it should have been deleted, it must be in {directory} but not in {expect}
#
#   Example:
#
#   $tester->run_test(
#                    test_name => 'sample test',
#                    directory => {
#                                 dir1 => {
#                                         file_a => q{
#                                                       |#!/bin/sh
#                                                       |
#                                                       |froobnagle() {
#                                                       |   froob a bit
#                                                       |   naggle even more
#                                                       |}
#                                                       |jimtoxy () {
#                                                       |   hi jim
#                                                       |}
#                                                    }
#                                         }
#                                 }
#                    expect    => {
#                                 dir1 => {
#                                         file_a => {
#                                                   language  => 'shell',
#                                                   tag_names => [ qw( froobnagle jimtoxy )],
#                                                   }
#                                         }
#                                 tags => qq{
#                                           !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
#                                           !_TAG_FILE_SORTED\t2\tcase insensitive order
#                                           froobnagle\tdir1/file_a\t/^froobnagle()/
#                                           jimtoxy\tdir1/file_a\t/^jimtoxy\s\+()/
#                                           }
#                                 }
#                    );
#
sub run_test
    {
    my $self  = shift;
    my $param = { @_ };

    my $caller      = [ caller ];
    my $test_source = sprintf "(%s line %d)", @{$caller}[1,2];
    my $test_name   = $param->{test_name}
                   // "<unnamed test> $test_source";
    my $parsers     = $param->{parsers};    # optional test parsers
    my $verbosity   = $param->{verbosity};  # optional verbosity level
    my $cli_args    = $param->{cli_args};   # command line arguments to give to scan_cli_args()
    my $directory   = $param->{directory};  # virtual directory tree
    my $expect      = $param->{expect}      # expectations, with the same structure as {directory}
                   // {};

    my $label       = sub { sprintf "%s - %s %s", $test_name, $_[0], $test_source };

    #-----------------------------------------------------------------------
    #
    # Setup Test Environment
    #

    # Note whether we had a tags file initially or not.
    # Since Shtags::Test::FileSystem doesn't clone the tree, an unlink() would
    # actually remove it from $directory too.
    my $had_tags = exists $directory->{tags};

    # remove all indentation from scalar values in both $directory and $expect
    outdent_files( $directory );
    outdent_files( $expect    );

    # create a virtual file system
    my $file_system = Shtags::Test::FileSystem->new(
                                                   file_tree => $directory,
                                                   );

    # create a slightly modified Shtags object
    my $test_shtags = Shtags::Mock->new(
                                       parsers     => $parsers,
                                       verbosity   => $verbosity,
                                       file_system => $file_system,
                                       );

    #-----------------------------------------------------------------------
    #
    # Start indexing
    #

    #
    # Run shtags using our Mock object
    #
    $test_shtags->read_tags_file();
    $test_shtags->scan_cli_args( @{ $cli_args // [] } );
    $test_shtags->write_tags_file();

    #
    # Walk the {expect} tree looking for files in the {directory} file system
    #
    # Parse each file step-by-step, allowing various aspects of the parsing
    # process to be tested
    #
    my @dir_stack;
    my @path_stack;
    my $dir   = $expect;
    my $names = [ sort grep { $_ ne 'tags' } keys %{$dir} ];
    while( my $name = shift @{$names} )
        {
        push @path_stack, $name;
        my $path = join( '/', @path_stack );
        my $item = $dir->{$name};

        if( $file_system->is_dir( $path ) )
            {
            # enter sub-directory
            push @dir_stack, { dir => $dir, names => $names };
            $dir   = $dir->{$name};
            $names = [ sort keys %{$dir} ];
            }
        else
            {
            # process file

            exists $item->{language} and is(
                                           $test_shtags->{test}{parser_for_file}{$path},
                                           $item->{language},
                                           $label->( "language detection for $path" )
                                           );

            exists $item->{tag_names} and eq_or_diff(
                                                    [ sort $test_shtags->file_index_tag_names( $path ) ],
                                                    [ sort @{$item->{tag_names}} ],
                                                    $label->( "tags found in $path" )
                                                    );

            pop @path_stack;
            }
        while( not @{$names} and @dir_stack )
            {
            # leave directory
            pop @path_stack;
            my $frame = pop @dir_stack;
            $dir   = $frame->{dir};
            $names = $frame->{names};
            }
        }

    #
    # test written tags
    #
    if( $had_tags or exists $expect->{tags} )
        {
        my $tags_after    = $directory->{tags};
        my $tags_expected = $expect->{tags};

        is(
            $tags_after    ? 'yes' : 'no',
            $tags_expected ? 'yes' : 'no',
            $label->( "tags file existence" ),
            );

        eq_or_diff(
            $tags_after,
            $tags_expected,
            $label->( "tags file contents" ),
            );
        }

    }

sub test_language
    {
    my $test_shtags = shift;
    my $path = shift;
    my $expect = shift;

    }

sub outdent_files
    {
    my $dir = shift;

    foreach my $name ( keys %{$dir} )
        {
        my $item = $dir->{$name};
        if( $item )
            {
            if( ref $item eq 'HASH' )
                {
                outdent_files( $item );
                }
            elsif( not ref $item )
                {
                $item =~ s/^\h+\|?//gm; # remove all indentation, up-to-and-including an optional '|' marker
                $item =~ s/\A\v+//gm;   # remove all leading newlines
                $item =~ s/\v+\z/\n/gm; # remove all but one trailing newlines
                $dir->{$name} = $item;  # replace item in directory tree (s/// uses copy-on-write)
                }
            }
        }

    return;
    }

1;
