package Shtags::Test::FileSystem;

#
# A mock file system for testing shtags
#
# Shtags::FileSystem in the shtags script is a mini-proxy object so that we can
# mock file system operations used by shtags
#
# Implementation Notes:
#
#   file system represented by a simple hash
#
#       path components are keys in nested hashes
#       directories are hashrefs
#       files are scalars
#

use strict;
use warnings;

use POSIX;  # get errno constants so we can emulate a file system
            #   EACCES  Permission denied
            #   EBADF   Bad file descriptor
            #   ENOENT  No such file or directory
            #   ENOTDIR Not a directory

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;
    $self->{file_tree} = $param->{file_tree} // {};

    $self->link_tree();     # add links for ./ and ../

    return $self;
    }

#
# API used by shtags

sub is_dir
    {
    my $self = shift;
    my $path = shift;

    my $item = $self->path_item( $path );

    return ( $item and ref $item eq 'HASH' ) ? 1 : 0;
    }

sub is_file
    {
    my $self = shift;
    my $path = shift;

    my $item = $self->path_item( $path );

    return ( defined $item and not ref $item ) ? 1 : 0;
    }

sub dir_items
    {
    my $self     = shift;
    my $dir_path = shift;

    my $dir = $self->path_item( $dir_path );

    return set_errno( EBADF ) if not $dir;
    return set_errno( EBADF ) if not ref $dir eq 'HASH';

    return keys %{$dir};
    }

sub unlink
    {
    my $self = shift;
    my $path = shift;

    my ( $dir_name, $file_name ) = map { $_ // '' }
                                   ( $path =~ m{(.*/)?([^/]+)$} );

    my $dir = $self->path_item( $dir_name );

    return if not $dir;
    return set_errno( ENOTDIR ) if not ref $dir eq 'HASH';

    return defined delete $dir->{$file_name};
    }

sub open_file
    {
    my $self = shift;
    my $mode = shift;
    my $path = shift;

    my ( $dir_name, $file_name ) = map { $_ // '' }
                                   ( $path =~ m{(.*/)?([^/]+)$} );

    my $dir = $self->path_item( $dir_name );

    return set_errno( ENOENT )  if not ref $dir;

    if( $mode =~ /</ )
        {
        # reading assumes that the file exists
        return                      if not defined $dir->{$file_name};
        return set_errno( ENOENT )  if ref $dir->{$file_name};
        }
    elsif( $mode =~ />/ )
        {
        # don't create files over directories
        return set_errno( EACCES )  if ref $dir->{$file_name};
        $dir->{$file_name} = '';    # give open something to attach to
        }

    #
    # open a file handle to our in-memory file
    #
    # Note: need to provide reference to original hash value
    #   e.g.
    #       $file = $dir->{$file_name};
    #       open(...)
    #
    #   ... only changes $file - $dir->{$file_name} is left unchanged
    #
    open( my $handle, $mode, \$dir->{$file_name} );

    return $handle;
    }

#
# Internal routines
#

sub set_errno { $! = shift; return; }

# link all directories to their parents so that we can emulate ./ and ../
sub link_tree
    {
    my $self   = shift;
    my $dir    = shift // $self->{file_tree};
    my $parent = shift;

    $dir->{'.'}  = $dir;
    $dir->{'..'} = $parent // $dir;

    foreach my $name ( keys %{$dir} )
        {
        next if $name =~ /^\.\.?$/; # skip ./ and ../ if they already exist

        my $subdir = $dir->{$name};
        next if ref $subdir ne 'HASH';

        $self->link_tree( $subdir, $dir );
        }

    return;
    }

sub path_item
    {
    my $self = shift;
    my @path = split( m{/+}, shift );

    my $item = $self->{file_tree};
    while( my $name = shift( @path ) )
        {
        next if $name eq '.';
        $item = $item->{$name};
        last if not ref $item eq 'HASH';
        }

    return set_errno( ENOTDIR ) if @path;
    return set_errno( ENOENT  ) if not defined $item;

    return $item;
    }

1;
