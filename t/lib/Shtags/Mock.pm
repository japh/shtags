package Shtags::Mock;

# Mock Shtags object
#
# subclass of Shtags with a few overrides to aide testing

use strict;
use warnings;

our @ISA = qw( Shtags );
our $parsers;   # get access to $parsers from shtags

BEGIN
    {
    # use @INC to load 'shtags' into the 'Shtags::Test' namespace for testing
    # do implicitly loads all of shtags sub's into our namespace, i.e. Shtags::Test
    do 'shtags'
        or die "Cannot load main shtags script for testing - $!\n";
    }

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = $class->SUPER::new(
                                 # $parsers snatched from shtags script via 'do' above
                                 parsers     => $param->{parsers}   // $parsers,
                                 # verbosity = -1, total silence
                                 verbosity   => $param->{verbosity} // -1,
                                 # fake file system for testing - see Shtags::Tester
                                 file_system => $param->{file_system},
                                 );

    # additional buffer for capturing behavioural statistics
    # used for testing only
    $self->{test} = {
                    squawks         => [],
                    scanned_dir     => {},  # <dir path>  => <count>
                    scanned_file    => {},  # <file path> => <count>
                    discarded       => {},  # <file path> => <count>
                    parser_for_file => {},  # <file path> => <language>
                    file_tags       => {},  # <file path> => { <tag name> => <locator pattern> }
                    };

    return $self;
    }

# catch all squawk calls instead of sending them to STDERR
sub squawk
    {
    my $self = shift;
    $self->SUPER::squawk( @_ );
    push @{$self->{tests}{squawks}}, sprintf( "%-8s %s", shift, sprintf( shift, @_ ) );
    return;
    }

sub squawks { return @{$_[0]->{test}{squawks}} }

# override Shtags::discard_stale_tags
sub discard_stale_tags
    {
    my $self = shift;

    my $before = { map { $_ => undef } keys %{$self->{file} } };

    $self->SUPER::discard_stale_tags( @_ );

    my $after  = { map { $_ => undef } keys %{$self->{file} } };

    foreach my $discarded_file ( grep { not exists $after->{$_} } sort keys %{$before} )
        {
        $self->{test}{discarded}{$discarded_file}++;
        }

    return;
    }

# override Shtags::scan_dir_for_tags
sub scan_dir_for_tags
    {
    my $self     = shift;
    my $dir_path = $_[0];  # don't shift - just look :-)

    $self->{test}{scanned_dir}{$dir_path}++;

    return $self->SUPER::scan_dir_for_tags( @_ );
    }

# override Shtags::scan_file_for_tags
sub scan_file_for_tags
    {
    my $self      = shift;
    my $file_path = $_[0];  # don't shift - just look :-)

    $self->{test}{scanned_file}{$file_path}++;

    return $self->SUPER::scan_file_for_tags( @_ );
    }

# override Shtags::parser_for_file
sub parser_for_file
    {
    my $self      = shift;
    my $file_path = $_[0];  # don't shift - just look :-)

    my $parser = $self->SUPER::parser_for_file( @_ );

    $self->{test}{parser_for_file}{$file_path} = ( ref $parser eq 'HASH' )
                                                    ? $parser->{language}
                                                    : $parser;

    return $parser;
    }

# override Shtags::format_tag_line
sub format_tag_line
    {
    my $self  = shift;
    my $param = { @_ };

    my $file_path = $param->{file_path};
    my $tag_name  = $param->{tag_name};
    my $locator   = $param->{locator};

    my $tag_line  = $self->SUPER::format_tag_line( @_ );

    $self->add_tag_to_file_index(
                                path     => $file_path,
                                tag_name => $tag_name,
                                locator  => $tag_line =~ s{^[^\t]+\t[^\t]+\t}{}r,
                                );

    return $tag_line;
    }

sub add_tag_to_file_index
    {
    my $self  = shift;
    my $param = { @_ };

    my $path     = $param->{path};
    my $tag_name = $param->{tag_name};
    my $locator  = $param->{locator};

    push @{ $self->{test}{file_tags}{$path} //= [] },
            {
            tag_name => $tag_name,
            locator  => $locator =~ s{^[^\t]+\t[^\t]+\t}{}r,
            };

    }

sub file_index_tag_names
    {
    my $self = shift;
    my $path = shift;

    return map { $_->{tag_name} }
           @{ $self->{test}{file_tags}{$path} //= [] };
    }

1;
