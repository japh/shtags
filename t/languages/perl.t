#!/usr/bin/env perl

# Unit tests against the 'perl' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'perl parser',
                 directory => {
                              with_suffix     => {
                                                 'test'    => '',
                                                 'test.pl' => '',
                                                 'test.PL' => '',
                                                 'test.pm' => '',
                                                 'test.t'  => '',
                                                 },
                              without_suffix  => {
                                                 'magic_1' => '#!perl',
                                                 'magic_2' => '#!/bin/perl',
                                                 'magic_3' => '#!/usr/bin/perl',
                                                 'magic_4' => '#!/usr/local/bin/perl',
                                                 'magic_5' => '#!/usr/bin/env perl',
                                                 'magic_6' => '#!/usr/bin/env perl -w',
                                                 'magic_7' => '#!/usr/bin/env perl -w',
                                                 'magic_8' => '#!/usr/bin/pearl',
                                                 'magic_9' => 'package Foo::Bar;',
                                                 'magic_a' => 'package   Foo::Bar  ;',
                                                 'x.java'  => 'package foo.bar;',
                                                 },
                              files_with_tags => {
                                                 'blah.pl' => q{

                                                               |#!/usr/bin/perl
                                                               |
                                                               |sub do_madgicks
                                                               |   {
                                                               |   if( @_ )
                                                               |       {
                                                               |       # sub reveal should be ignored here
                                                               |       return map  { $_->[0] }
                                                               |              sort {
                                                               |                      $a->[1] cmp $b->[1]
                                                               |                   or $a->[2] <=> $b->[2]
                                                               |                   }
                                                               |              map  { [ $_, $_->{a}, $_->{b} ] }
                                                               |              @_
                                                               |       }
                                                               |   }
                                                               |
                                                               |# nothing wrong with having duplicate tags within a file
                                                               |# but should probably not allow duplicate tags within a module
                                                               |# (but that's what perl -c does - we're just here for the index)
                                                               |
                                                               |# known issue: two 'identical' lines only produce 1 tag
                                                               |sub new
                                                               |sub new
                                                               |
                                                               |# a slightly different line is OK though
                                                               |sub new \{ # note: need \\ here because we are in a q{} block

                                                               },
                                                 'blup.pm' => q{

                                                               #!/usr/bin/perl

                                                               package Blup::Diddly::Test;

                                                               sub some_rubbish
                                                                  {
                                                                  if( @_ )
                                                                      {
                                                                      # sub foo() should be ignored here
                                                                      return map  { $_->[0] }
                                                                             sort {
                                                                                     $a->[1] cmp $b->[1]
                                                                                  or $a->[2] <=> $b->[2]
                                                                                  }
                                                                             map  { [ $_, $_->{a}, $_->{b} ] }
                                                                             @_
                                                                      }
                                                                  }
                                                               },
                                                 'moose.pm' => q{
                                                                package Some::Moose::Class;

                                                                use Moo;

                                                                has moo_foo     => ( is => 'rw', isa => Str );

                                                                has 'moo_name'  => ( is => 'rw', isa => Str );

                                                                has "moo_label" => ( is => 'rw', isa => Str );

                                                                },
                                                 },
                              },
                 expect    => {
                              with_suffix     => {
                                                 'test'    => { language => undef  },
                                                 'test.pl' => { language => 'perl' },
                                                 'test.PL' => { language => 'perl' },
                                                 'test.pm' => { language => 'perl' },
                                                 'test.t'  => { language => 'perl' },
                                                 },
                              without_suffix  => {
                                                 'magic_1' => { language => 'perl' },
                                                 'magic_2' => { language => 'perl' },
                                                 'magic_3' => { language => 'perl' },
                                                 'magic_4' => { language => 'perl' },
                                                 'magic_5' => { language => 'perl' },
                                                 'magic_6' => { language => 'perl' },
                                                 'magic_7' => { language => 'perl' },
                                                 'magic_8' => { language => undef  },
                                                 'magic_9' => { language => 'perl' },
                                                 'magic_a' => { language => 'perl' },
                                                 'x.java'  => { language => undef  },
                                                 },
                              files_with_tags => {
                                                 'blah.pl' => {
                                                              language  => 'perl',
                                                              # note: three "sub new"'s found
                                                              # but one duplicate is removed
                                                              # from tags file (later)
                                                              tag_names => [ qw(
                                                                               do_madgicks
                                                                               new
                                                                               new
                                                                               new
                                                                            ) ],
                                                             },
                                                 'blup.pm' => {
                                                              language  => 'perl',
                                                              tag_names => [ qw(
                                                                               Blup::Diddly::Test
                                                                               some_rubbish
                                                                            ) ],
                                                             },
                                                 },
                              tags => qq{
                                        !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                        !_TAG_FILE_SORTED\t2\tcase insensitive order
                                        Blup::Diddly::Test\tfiles_with_tags/blup.pm\t/^package\\s\\+Blup::Diddly::Test;/
                                        do_madgicks\tfiles_with_tags/blah.pl\t/^sub\\s\\+do_madgicks\\>/
                                        Foo::Bar\twithout_suffix/magic_9\t/^package\\s\\+Foo::Bar;/
                                        Foo::Bar\twithout_suffix/magic_a\t/^package\\s\\+Foo::Bar\\s\\+;/
                                        moo_foo\tfiles_with_tags/moose.pm\t/^has\\s\\+moo_foo\\s\\+=>\\s\\+(\\s\\+is\\s\\+=>\\s\\+\'rw\',\\s\\+isa\\s\\+=>\\s\\+Str\\s\\+);/
                                        moo_label\tfiles_with_tags/moose.pm\t/^has\\s\\+"moo_label"\\s\\+=>\\s\\+(\\s\\+is\\s\\+=>\\s\\+\'rw\',\\s\\+isa\\s\\+=>\\s\\+Str\\s\\+);/
                                        moo_name\tfiles_with_tags/moose.pm\t/^has\\s\\+\'moo_name\'\\s\\+=>\\s\\+(\\s\\+is\\s\\+=>\\s\\+\'rw\',\\s\\+isa\\s\\+=>\\s\\+Str\\s\\+);/
                                        new\tfiles_with_tags/blah.pl\t/^sub\\s\\+new\\>/
                                        new\tfiles_with_tags/blah.pl\t/^sub\\s\\+new\\s\\+\{/
                                        Some::Moose::Class\tfiles_with_tags/moose.pm\t/^package\\s\\+Some::Moose::Class;/
                                        some_rubbish\tfiles_with_tags/blup.pm\t/^sub\\s\\+some_rubbish\\>/
                                        },
                              },
                 );

exit;
