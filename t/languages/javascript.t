#!/usr/bin/env perl

# Unit tests against the 'javascript' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'javascript parser',
                 directory => {
                              with_suffix     => {
                                                 'test'    => '',
                                                 'test.js' => '',
                                                 },
                              files_with_tags => {
                                                 'classy.js' => q{

                                                                 |// look at this crap!
                                                                 |//    Which of the following are methods to be tagged?
                                                                 |//        constructor(...
                                                                 |//        for(...
                                                                 |//        if(...
                                                                 |//        my_magical_method(...
                                                                 |//    see also 'normal' javascript - it all looks the same
                                                                 |// (if you don't want to actually parse the code with a proper JS parser)
                                                                 |export default class Chart {
                                                                 |
                                                                 |    constructor( container ) {
                                                                 |        this.series      = new Set();
                                                                 |        this.setup_svg();
                                                                 |    }
                                                                 |
                                                                 |    setup_svg() {
                                                                 |        // javascript often contains chunks of HTML
                                                                 |        this.c_el.innerHTML = `
                                                                 |                              <svg class=chart>
                                                                 |                                  <svg class=canvas width=100 height=100 viewBox="0 0 100 100">
                                                                 |                                      <circle cx=20 cy=20 r=5 stroke=black fill=green />
                                                                 |                                  </svg>
                                                                 |                              </svg>
                                                                 |                              `;
                                                                 |    }
                                                                 |
                                                                 |    get series() { return this.series; }
                                                                 |
                                                                 |    static extent( data, func ) {
                                                                 |        if( ! data || ! data.length ) { return }
                                                                 |        console.log( "getting extent of some data", { data } );
                                                                 |        const x0 = func( data[0] );
                                                                 |        let min = x0;
                                                                 |        let max = x0;
                                                                 |        for( let i = 1; i < data.length; i++ ) {
                                                                 |            const x = func( data[i] );
                                                                 |            if( x < min ) { min = x }
                                                                 |            if( x > max ) { max = x }
                                                                 |        }
                                                                 |        console.log( "got min max", [ min, max ] );
                                                                 |        return [ min, max ];
                                                                 |    }
                                                                 |}

                                                                 },
                                                 'trashy.js' => q{

                                                                 |export default MyModule;
                                                                 |
                                                                 |function MyModule( foo, bar ) {
                                                                 |                              ...
                                                                 |                              }
                                                                 |
                                                                 |function munge( foo, bar ) {
                                                                 |                           ...
                                                                 |                           }

                                                                 },
                                                 },
                              },
                 expect    => {
                              with_suffix     => {
                                                 'test'    => { language => undef  },
                                                 'test.js' => { language => 'javascript' },
                                                 },
                              without_suffix  => {
                                                 },
                              files_with_tags => {
                                                 'classy.js' => {
                                                              language  => 'javascript',
                                                              tag_names => [ qw(
                                                                               constructor
                                                                               setup_svg
                                                                               series
                                                                               extent
                                                                               ) ],
                                                             },
                                                 'trashy.js' => {
                                                              language  => 'javascript',
                                                              tag_names => [ qw(
                                                                               MyModule
                                                                               munge
                                                                               ) ],
                                                             },
                                                 },
                              tags => qq{
                                        !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                        !_TAG_FILE_SORTED\t2\tcase insensitive order
                                        constructor\tfiles_with_tags/classy.js\t/^\\s\\+constructor(/
                                        extent\tfiles_with_tags/classy.js\t/^\\s\\+static\\s\\+extent(/
                                        munge\tfiles_with_tags/trashy.js\t/^function\\s\\+munge(/
                                        MyModule\tfiles_with_tags/trashy.js\t/^function\\s\\+MyModule(/
                                        series\tfiles_with_tags/classy.js\t/^\\s\\+get\\s\\+series(/
                                        setup_svg\tfiles_with_tags/classy.js\t/^\\s\\+setup_svg(/
                                        },
                              },
                 );

exit;
