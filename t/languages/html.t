#!/usr/bin/env perl

# Unit tests against the 'shell' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'html parser detection',
                 directory => {
                              with_suffix    => {
                                                'test'      => '',
                                                'test.html' => '',
                                                'test.htm'  => '',
                                                },
                              without_suffix => {
                                                'magic_1' => '<!doctype html>',
                                                'magic_2' => '<html>',
                                                },
                              },
                 expect    => {
                              with_suffix    => {
                                                'test'      => { language => undef  },
                                                'test.html' => { language => 'html' },
                                                'test.htm'  => { language => 'html' },
                                                },
                              without_suffix => {
                                                'magic_1' => { language => 'html' },
                                                'magic_2' => { language => 'html' },
                                                },
                              },
                 );

$tester->run_test(
                 test_name => 'html indexing',
                 directory => {
                              'index.html' => q{

                                               |<!doctype html>
                                               |<html>
                                               |<head>
                                               |</head>
                                               |<body>
                                               |
                                               |    <nav id=nav>
                                               |        <ul>
                                               |            <li><a href="#foo">Click Here</a></li>
                                               |        </ul>
                                               |    </nav>
                                               |    <section id = section-one ></section>
                                               |    <article id= "article two" ></section>
                                               |
                                               |</body>
                                               |</html>

                                              },
                              },
                 expect    => {
                              'index.html' =>  {
                                               language  => 'html',
                                               tag_names => [ qw( nav section-one ), 'article two' ],
                                               },
                              tags        => qq{
                                               !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                               !_TAG_FILE_SORTED\t2\tcase insensitive order
                                               article two\tindex.html\t/^\\s\\+<article\\s\\+id=\\s\\+"article\\s\\+two"/
                                               nav\tindex.html\t/^\\s\\+<nav\\s\\+id=nav\\>/
                                               section-one\tindex.html\t/^\\s\\+<section\\s\\+id\\s\\+=\\s\\+section-one\\>/
                                               },
                              },
                 );

exit;

