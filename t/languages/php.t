#!/usr/bin/env perl

# Unit tests against the 'php' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'php parser',
                 directory => {
                              with_suffix     => {
                                                 'test'     => '',
                                                 'test.php' => '',
                                                 },
                              without_suffix  => {
                                                 'test'     => q{
                                                                
                                                                |#!/usr/bin/php -q
                                                                |<? some php code ?>

                                                                },
                                                 },
                              files_with_tags => {
                                                 'simple.php' => q{

                                                                 |function munge( $foo, $bar ) {
                                                                 |                             ...
                                                                 |                             }

                                                                 },
                                                 },
                              },
                 expect    => {
                              with_suffix     => {
                                                 'test'     => { language => undef },
                                                 'test.php' => { language => 'php' },
                                                 },
                              without_suffix  => {
                                                 'test'     => { language => 'php' },
                                                 },
                              files_with_tags => {
                                                 'simple.php' => {
                                                              language  => 'php',
                                                              tag_names => [ qw(
                                                                               munge
                                                                               ) ],
                                                             },
                                                 },
                              tags => qq{
                                        !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                        !_TAG_FILE_SORTED\t2\tcase insensitive order
                                        munge\tfiles_with_tags/simple.php\t/^function\\s\\+munge(/
                                        },
                              },
                 );

exit;
