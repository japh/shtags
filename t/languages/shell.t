#!/usr/bin/env perl

# Unit tests against the 'shell' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'shell parser detection',
                 directory => {
                              with_suffix    => {
                                                'test'      => '',
                                                'test.sh'   => '',
                                                'test.bash' => '',
                                                'test.ksh'  => '',
                                                'test.zsh'  => '',
                                                },
                              without_suffix => {
                                                'magic_1' => '#!/bin/sh',
                                                'magic_2' => '#!/bin/bash',
                                                'magic_3' => '#!/bin/ksh',
                                                'magic_4' => '#!/bin/zsh',
                                                'magic_5' => '#!/usr/bin/ksh',
                                                'magic_6' => '#!/usr/local/bin/ksh93',
                                                'magic_7' => '#!/usr/bin/sh -x',
                                                },
                              },
                 expect    => {
                              with_suffix    => {
                                                'test'      => { language => undef   },
                                                'test.sh'   => { language => 'shell' },
                                                'test.bash' => { language => 'shell' },
                                                'test.ksh'  => { language => 'shell' },
                                                'test.zsh'  => { language => 'shell' },
                                                },
                              without_suffix => {
                                                'magic_1' => { language => 'shell' },
                                                'magic_2' => { language => 'shell' },
                                                'magic_3' => { language => 'shell' },
                                                'magic_4' => { language => 'shell' },
                                                'magic_5' => { language => 'shell' },
                                                'magic_6' => { language => 'shell' },
                                                'magic_7' => { language => 'shell' },
                                                },
                              tags => undef,
                              },
                 );

$tester->run_test(
                 test_name => 'shell indexing',
                 directory => {
                              'script.sh' => q{

                                              #!/bin/ksh

                                              blah blah

                                              one ( ) {   # space between () not allowed
                                              }

                                              two () {   # space between function name and () not allowed
                                              }

                                              three() {   # this should work
                                              }

                                              four()     # \{ needs to follow ()
                                              {
                                              }

                                              },
                              },
                 expect    => {
                              'script.sh' =>  {
                                              language  => 'shell',
                                              tag_names => [ qw( three ) ],
                                              },
                              tags        => qq{
                                               !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                               !_TAG_FILE_SORTED\t2\tcase insensitive order
                                               three\tscript.sh\t/^three()\\s\\+\{/
                                               },
                              },
                 );

exit;
