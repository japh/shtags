#!/usr/bin/env perl

# Unit tests against the 'elm' tags parser

use v5.18;
use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";     # for Shtags::Test
use lib "$RealBin/../..";      # for shtags

use Shtags::Tester;

my $tester = Shtags::Tester->new();

$tester->run_test(
                 test_name => 'elm parser',
                 directory => {
                              with_suffix     => {
                                                 'test'     => '',
                                                 'test.elm' => '',
                                                 },
                              files_with_tags => {
                                                 'buttons.elm' => q{

                                                                   |module Main exposing (..)
                                                                   |
                                                                   |-- Press buttons to increment and decrement a counter.
                                                                   |--
                                                                   |-- Read how it works:
                                                                   |--   https://guide.elm-lang.org/architecture/buttons.html
                                                                   |--
                                                                   |
                                                                   |
                                                                   |import Browser
                                                                   |import Html exposing (Html, button, div, text)
                                                                   |import Html.Events exposing (onClick)
                                                                   |
                                                                   |
                                                                   |
                                                                   |-- MAIN
                                                                   |
                                                                   |
                                                                   |main =
                                                                   |  Browser.sandbox { init = init, update = update, view = view }
                                                                   |
                                                                   |
                                                                   |
                                                                   |-- MODEL
                                                                   |
                                                                   |
                                                                   |type alias Model = Int
                                                                   |
                                                                   |
                                                                   |init : Model
                                                                   |init =
                                                                   |  0
                                                                   |
                                                                   |
                                                                   |
                                                                   |-- UPDATE
                                                                   |
                                                                   |
                                                                   |type Msg
                                                                   |  = Increment
                                                                   |  | Decrement
                                                                   |
                                                                   |
                                                                   |update : Msg -> Model -> Model
                                                                   |update msg model =
                                                                   |  case msg of
                                                                   |    Increment ->
                                                                   |      model + 1
                                                                   |
                                                                   |    Decrement ->
                                                                   |      model - 1
                                                                   |
                                                                   |
                                                                   |
                                                                   |-- VIEW
                                                                   |
                                                                   |
                                                                   |view : Model -> Html Msg
                                                                   |view model =
                                                                   |  div []
                                                                   |    [ button [ onClick Decrement ] [ text "-" ]
                                                                   |    , div [] [ text (String.fromInt model) ]
                                                                   |    , button [ onClick Increment ] [ text "+" ]
                                                                   |    ]
                                                                   |

                                                                   },
                                                 },
                              },
                 expect    => {
                              with_suffix     => {
                                                 'test'     => { language => undef },
                                                 'test.elm' => { language => 'elm' },
                                                 },
                              files_with_tags => {
                                                 'buttons.elm' => {
                                                                  language  => 'elm',
                                                                  tag_names => [ qw(
                                                                                   Main
                                                                                   main
                                                                                   Model
                                                                                   init
                                                                                   Msg
                                                                                   Increment
                                                                                   Decrement
                                                                                   update
                                                                                   view
                                                                                   ) ],
                                                                  },
                                                 },
                              tags => qq{
                                        !_TAG_FILE_ENCODING\tutf-8\tutf-8 encoding
                                        !_TAG_FILE_SORTED\t2\tcase insensitive order
                                        Decrement\tfiles_with_tags/buttons.elm\t/^\\s\\+|\\s\\+Decrement\\>/
                                        Increment\tfiles_with_tags/buttons.elm\t/^\\s\\+=\\s\\+Increment\\>/
                                        init\tfiles_with_tags/buttons.elm\t/^init\\s\\+=/
                                        main\tfiles_with_tags/buttons.elm\t/^main\\s\\+=/
                                        Main\tfiles_with_tags/buttons.elm\t/^module\\s\\+Main\\s\\+exposing\\>/
                                        Model\tfiles_with_tags/buttons.elm\t/^type\\s\\+alias\\s\\+Model\\>/
                                        Msg\tfiles_with_tags/buttons.elm\t/^type\\s\\+Msg\\>/
                                        update\tfiles_with_tags/buttons.elm\t/^update\\s\\+msg\\s\\+model\\s\\+=/
                                        view\tfiles_with_tags/buttons.elm\t/^view\\s\\+model\\s\\+=/
                                        },
                              },
                 );

exit;
